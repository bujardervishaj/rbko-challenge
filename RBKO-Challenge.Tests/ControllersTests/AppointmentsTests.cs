﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using RBKO_Challenge.Controllers;
using RBKO_Challenge.Models;
using RBKO_Challenge.Services;
using RBKO_Challenge.Utilities;
using Newtonsoft.Json;

namespace RBKO_Challenge.Tests.ControllersTests
{
    [TestFixture]
    public class AppointmentsTests
    {
        [Test]
        public void ListOfAllAvailableAppointments_Success()
        {
            IAppointmentService appointmentService = new AppointmentService();
            var controller = new Appointments(appointmentService);

            var result = controller.ListOfAllAvailableAppointments(new ApiInputModel
            {
                WorkingSchedule = _testData.WorkingSchedule,
                BookedAppointments = _testData.BookedAppointments
            });

            var okResult = result as ObjectResult;
            var actual = okResult?.Value as List<AvailableAppointment>;

            okResult.Should().NotBeNull();
            okResult.Should().BeOfType<OkObjectResult>();
            actual?.Count.Should().Be(_expectedResult.Count);
            actual.Should().BeEquivalentTo(_expectedResult);
        }

        [Test]
        public void ListOfAllAvailableAppointments_Fail()
        {
            IAppointmentService appointmentService = new AppointmentService();
            var controller = new Appointments(appointmentService);

            var result = controller.ListOfAllAvailableAppointments(new ApiInputModel());

            result.Should().NotBeNull();
            result.Should().BeOfType<BadRequestResult>();
        }

        [Test]
        public void GetListOfAllAvailableAppointmentsFromQuery_Success()
        {
            IAppointmentService appointmentService = new AppointmentService();
            var controller = new Appointments(appointmentService);

            var result = controller.GetListOfAllAvailableAppointmentsFromQuery(JsonConvert.SerializeObject(_testData.WorkingSchedule), JsonConvert.SerializeObject(_testData.BookedAppointments));

            var okResult = result as ObjectResult;
            var actual = okResult?.Value as List<AvailableAppointment>;

            okResult.Should().NotBeNull();
            okResult.Should().BeOfType<OkObjectResult>();
            actual?.Count.Should().Be(_expectedResult.Count);
            actual.Should().BeEquivalentTo(_expectedResult);
        }

        [Test]
        public void GetListOfAllAvailableAppointmentsFromQuery_Fail()
        {
            IAppointmentService appointmentService = new AppointmentService();
            var controller = new Appointments(appointmentService);

            var result = controller.GetListOfAllAvailableAppointmentsFromQuery(null, null);

            result.Should().NotBeNull();
            result.Should().BeOfType<BadRequestResult>();
        }

        [Test]
        public void GetListOfAllAvailableAppointmentsByEmployeeId_Success()
        {
            var mockEmployeeRepo = new Mock<IEmployeeRepository>();
            mockEmployeeRepo.Setup(x => x.GetEmployeeById(Guid.Parse("a98121e8-2492-43b2-9bce-289163324c1f")))
                .Returns(_expectedEmployee);

            IAppointmentService appointmentService = new AppointmentService(mockEmployeeRepo.Object);
            var controller = new Appointments(appointmentService);

            var result = controller.GetListOfAllAvailableAppointmentsByEmployeeId(Guid.Parse("a98121e8-2492-43b2-9bce-289163324c1f"));

            var okResult = result as ObjectResult;
            var actual = okResult?.Value as List<AvailableAppointment>;

            okResult.Should().NotBeNull();
            okResult.Should().BeOfType<OkObjectResult>();
            actual?.Count.Should().Be(_expectedResult.Count);
            actual.Should().BeEquivalentTo(_expectedResult);
        }

        [Test]
        public void GetListOfAllAvailableAppointmentsByEmployeeId_Fail()
        {
            var mockEmployeeRepo = new Mock<IEmployeeRepository>();
            mockEmployeeRepo.Setup(x => x.GetEmployeeById(Guid.Parse("a98121e8-2492-43b2-9bce-289163324c0f")))
                .Returns((Employee)null);

            IAppointmentService appointmentService = new AppointmentService(mockEmployeeRepo.Object);
            var controller = new Appointments(appointmentService);

            var result = controller.GetListOfAllAvailableAppointmentsByEmployeeId(Guid.Parse("a98121e8-2492-43b2-9bce-289163324c0f"));

            result.Should().NotBeNull();
            result.Should().BeOfType<NotFoundResult>();
        }


        private readonly ApiInputModel _testData = new ApiInputModel
        {
            WorkingSchedule = new List<WorkingSchedule>
            {
                    new WorkingSchedule{Start = new DateTime(2020, 10, 1, 8, 0, 0), End = new DateTime(2020, 10, 1, 12, 0, 0)},
                    new WorkingSchedule{Start = new DateTime(2020, 10, 1, 13, 0, 0), End = new DateTime(2020, 10, 1, 16, 0, 0)}
            },
            BookedAppointments = new List<BookedAppointment>
            {
                    new BookedAppointment{Start = new DateTime(2020, 10, 1, 9, 0, 0), End = new DateTime(2020, 10, 1, 10, 0, 0)},
                    new BookedAppointment{Start = new DateTime(2020, 10, 1, 15, 30, 0), End = new DateTime(2020, 10, 1, 16, 0, 0)},
            }
        };

        private readonly List<AvailableAppointment> _expectedResult = new List<AvailableAppointment>
        {
            new AvailableAppointment {Start = new DateTime(2020, 10, 1, 8, 0, 0), End = new DateTime(2020, 10, 1, 9, 0, 0)},
            new AvailableAppointment {Start = new DateTime(2020, 10, 1, 10, 0, 0), End = new DateTime(2020, 10, 1, 12, 0, 0)},
            new AvailableAppointment {Start = new DateTime(2020, 10, 1, 13, 0, 0), End = new DateTime(2020, 10, 1, 15, 30, 0)},
        };

        private readonly Employee _expectedEmployee = new Employee
        {
            EmployeeId = Guid.Parse("a98121e8-2492-43b2-9bce-289163324c1f"),
            FirstName = "FirstName1",
            LastName = "LastName1",
            Email = "firstname1.lastname1@email.co",
            DOB = DateTime.Now.AddYears(-30),
            Address = "Address1",
            WorkingSchedules = new List<WorkingSchedule>
            {
                new WorkingSchedule{Start = new DateTime(2020, 10, 1, 8, 0, 0), End = new DateTime(2020, 10, 1, 12, 0, 0)},
                new WorkingSchedule{Start = new DateTime(2020, 10, 1, 13, 0, 0), End = new DateTime(2020, 10, 1, 16, 0, 0)}
            },
            BookedAppointments = new List<BookedAppointment>
            {
                new BookedAppointment{Start = new DateTime(2020, 10, 1, 9, 0, 0), End = new DateTime(2020, 10, 1, 10, 0, 0)},
                new BookedAppointment{Start = new DateTime(2020, 10, 1, 15, 30, 0), End = new DateTime(2020, 10, 1, 16, 0, 0)},
            }
        };
    }
}
