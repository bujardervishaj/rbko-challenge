﻿using System;
using System.Collections.Generic;
using RBKO_Challenge.Models;

namespace RBKO_Challenge.Services
{
    public interface IAppointmentService
    {
        IEnumerable<AvailableAppointment> Available(Guid employeeId);

        IEnumerable<AvailableAppointment> GetListOfAllAvaliableAppointments(IEnumerable<WorkingSchedule> workingSchedule,
            IEnumerable<BookedAppointment> bookedAppointments);

        IEnumerable<AvailableAppointment> GetListOfAllAvaliableAppointmentsFromJsonStrings(string workingSchedule,
            string bookedAppointments);
    }
}
