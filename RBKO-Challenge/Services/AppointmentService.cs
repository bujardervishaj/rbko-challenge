﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RBKO_Challenge.Models;
using RBKO_Challenge.Utilities;

namespace RBKO_Challenge.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public AppointmentService()
        {
        }

        public AppointmentService(IEmployeeRepository employeeRepository)
        {
            this._employeeRepository = employeeRepository;
        }

        public IEnumerable<AvailableAppointment> Available(Guid employeeId)
        {
            var employee = _employeeRepository.GetEmployeeById(employeeId);
            if (employee == null) return null;

            return GetListOfAllAvaliableAppointments(
                employee.WorkingSchedules.Where(x => x.Start >= DateTime.Now),
                employee.BookedAppointments.Where(x => x.Start >= DateTime.Now));
        }

        public IEnumerable<AvailableAppointment> GetListOfAllAvaliableAppointmentsFromJsonStrings(string workingSchedule,
            string bookedAppointments)
        {
            try
            {
                var localWorkingSchedule = JsonConvert.DeserializeObject<List<WorkingSchedule>>(workingSchedule);
                var localBookedAppointments = JsonConvert.DeserializeObject<List<BookedAppointment>>(bookedAppointments);
                return GetListOfAllAvaliableAppointments(localWorkingSchedule, localBookedAppointments);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AvailableAppointment> GetListOfAllAvaliableAppointments(IEnumerable<WorkingSchedule> workingSchedule, IEnumerable<BookedAppointment> bookedAppointments)
        {
            var availableSlots = workingSchedule.Select(x => new AvailableAppointment { Start = x.Start, End = x.End }).ToList();

            foreach (var booking in bookedAppointments)
            {
                var actualSchedule = availableSlots.FirstOrDefault(x =>
                    x.Start.Date.Equals(booking.Start.Date) && x.Start <= booking.Start && x.End >= booking.End);

                var newSlots = CalculateAvailableSlotsWithinSchedule(actualSchedule, booking);

                availableSlots.AddRange(newSlots);
                availableSlots.Remove(actualSchedule);
            }

            return availableSlots;
        }

        private IEnumerable<AvailableAppointment> CalculateAvailableSlotsWithinSchedule(AvailableAppointment actualWorkingSchedule, BookedAppointment booking)
        {
            var newSlots = new List<AvailableAppointment>();

            //divide schedule into new slots and delete the old one where the appointment is included
            if (actualWorkingSchedule?.Start < booking.Start)
            {
                newSlots.Add(new AvailableAppointment { Start = actualWorkingSchedule.Start, End = booking.Start });

                if (booking.End < actualWorkingSchedule.End)
                {
                    newSlots.Add(new AvailableAppointment { Start = booking.End, End = actualWorkingSchedule.End });
                }
            }
            else if (actualWorkingSchedule?.End > booking.End)
            {
                newSlots.Add(new AvailableAppointment { Start = booking.End, End = actualWorkingSchedule.End });
            }

            return newSlots;
        }
    }
}
