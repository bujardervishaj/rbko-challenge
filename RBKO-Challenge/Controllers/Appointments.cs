﻿using System;
using Microsoft.AspNetCore.Mvc;
using RBKO_Challenge.Services;
using RBKO_Challenge.Utilities;

namespace RBKO_Challenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Appointments : ControllerBase
    {
        private readonly IAppointmentService _appointmentService;

        public Appointments(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        /// <summary>
        /// List of all available appointments from provided objects.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Appointments/Available
        ///	    {
        ///	    	"workingSchedule": [
        ///	    		{
        ///	    			"start": "2020-04-24 08:00:00",
        ///	    			"end": "2020-04-24 12:00:00"
        ///	    		},
        ///	    		{
        ///	    			"start": "2020-04-24 13:00:00",
        ///	    			"end": "2020-04-24 16:00:00"
        ///	    		}
        ///	    	],
        ///	    	"bookedAppointments": [
        ///	    		{
        ///	    			"start": "2020-04-24 09:00:00",
        ///	    			"end": "2020-04-24 10:00:00"
        ///	    		},
        ///	    		{
        ///	    			"start": "2020-04-24 15:30:00",
        ///	    			"end": "2020-04-24 16:00:00"
        ///	    		}
        ///	    	]
        ///	    }
        ///
        /// </remarks>
        /// <param name="inputModel"></param>
        /// <returns>List of Available appointments</returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>        
        [HttpPost]
        [Route("available")]
        public IActionResult ListOfAllAvailableAppointments(ApiInputModel inputModel)
        {
            if (inputModel.WorkingSchedule is null || inputModel.BookedAppointments is null || inputModel.WorkingSchedule.Count == 0)
                return BadRequest();
            return Ok(_appointmentService.GetListOfAllAvaliableAppointments(inputModel.WorkingSchedule, inputModel.BookedAppointments));
        }

        /// <summary>
        /// List of all available appointments from provided objects as JSON strings.
        /// </summary>
        /// <param name="workingSchedule">List of working schedule as JSON string</param>
        /// <param name="bookedAppointments">List of booked appointments as JSON string</param>
        /// <returns>List of Available appointments</returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>  
        [HttpGet]
        [Route("available")]
        public IActionResult GetListOfAllAvailableAppointmentsFromQuery([FromQuery]string workingSchedule, [FromQuery]string bookedAppointments)
        {
            var availableAppointments = _appointmentService.GetListOfAllAvaliableAppointmentsFromJsonStrings(workingSchedule, bookedAppointments);
            if (availableAppointments is null)
                return BadRequest();
            return Ok(availableAppointments);
        }

        /// <summary>
        /// List of all available appointments for employee.
        /// </summary>
        /// <param name="employeeId">ID of employee</param>
        /// <returns>List of Available appointments</returns>
        /// <response code="200">Success</response>
        /// <response code="404">Not Found</response> 
        [HttpGet]
        [Route("available/{employeeId}")]
        public IActionResult GetListOfAllAvailableAppointmentsByEmployeeId(Guid employeeId)
        {
            var availableAppointments = _appointmentService.Available(employeeId);
            if (availableAppointments is null)
                return NotFound();
            return Ok(availableAppointments);
        }
    }
}
