﻿using System;
using RBKO_Challenge.Models;

namespace RBKO_Challenge.Utilities
{
    public interface IEmployeeRepository
    {
        public Employee GetEmployeeById(Guid id);
    }
}
