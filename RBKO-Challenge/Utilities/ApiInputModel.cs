﻿using System.Collections.Generic;
using RBKO_Challenge.Models;

namespace RBKO_Challenge.Utilities
{
    public class ApiInputModel
    {
        public List<WorkingSchedule> WorkingSchedule { get; set; }

        public List<BookedAppointment> BookedAppointments { get; set; }

    }
}
