﻿using System;
using System.Collections.Generic;
using System.Linq;
using RBKO_Challenge.Models;

namespace RBKO_Challenge.Utilities
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly List<Employee> _dumbData = new List<Employee>
        {
            new Employee
            {
                EmployeeId = Guid.Parse("a98121e8-2492-43b2-9bce-289163324c0f"),
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "firstname1.lastname1@email.co",
                DOB = DateTime.Now.AddYears(-30),
                Address = "Address1",
                WorkingSchedules = new List<WorkingSchedule>
                {
                    new WorkingSchedule{Start = new DateTime(2020, 10, 1, 8, 0, 0), End = new DateTime(2020, 10, 1, 12, 0, 0)},
                    new WorkingSchedule{Start = new DateTime(2020, 10, 1, 13, 0, 0), End = new DateTime(2020, 10, 1, 16, 0, 0)}
                },
                BookedAppointments = new List<BookedAppointment>
                {
                    new BookedAppointment{Start = new DateTime(2020, 10, 1, 9, 0, 0), End = new DateTime(2020, 10, 1, 10, 0, 0)},
                    new BookedAppointment{Start = new DateTime(2020, 10, 1, 15, 30, 0), End = new DateTime(2020, 10, 1, 16, 0, 0)},
                }
            }
        };

        public Employee GetEmployeeById(Guid id)
        {
            return _dumbData.FirstOrDefault(x => x.EmployeeId.Equals(id));
        }
    }
}
