﻿using System;
using System.Collections.Generic;

namespace RBKO_Challenge.Models
{
    public class Employee
    {
        public Guid EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime DOB { get; set; }

        public string Address { get; set; }

        public List<WorkingSchedule> WorkingSchedules { get; set; }

        public List<BookedAppointment> BookedAppointments { get; set; }
    }
}
