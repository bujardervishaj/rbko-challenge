﻿using System;

namespace RBKO_Challenge.Models
{
    public abstract class Schedule
    {
        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
