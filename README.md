# RBKO-Challenge REST API

This solution provides a REST API for RBKO coding task:  
* Get list of all avaliable appointments

# REST API

The REST API to the example app is described below, also is documented with SWAGGER (Run the app and navigate to swagger/index.html)

## Get list of all avaliable appointments by providing a object where the List<WorkingSchedule> and the List<BookedAppointmets> is wrapped

### Request

`POST /appointments/available`

    curl -X POST "https://localhost:44382/Appointments/available" -H "accept: */*" -H "Content-Type: application/json-patch+json" -d "{\"workingSchedule\":[{\"start\":\"2020-04-24 08:00:00\",\"end\":\"2020-04-24 12:00:00\"},{\"start\":\"2020-04-24 13:00:00\",\"end\":\"2020-04-24 16:00:00\"}],\"bookedAppointments\":[{\"start\":\"2020-04-24 09:00:00\",\"end\":\"2020-04-24 10:00:00\"},{\"start\":\"2020-04-24 15:30:00\",\"end\":\"2020-04-24 16:00:00\"}]}"

### Response

    content-length: 181 
    content-type: application/json; charset=utf-8 
    date: Sat, 02 May 2020 11:23:56 GMT 
    server: Microsoft-IIS/10.0 
    status: 200 
    x-powered-by: ASP.NET 

    [
      {
        "start": "2020-04-24T08:00:00",
        "end": "2020-04-24T09:00:00"
      },
      {
        "start": "2020-04-24T10:00:00",
        "end": "2020-04-24T12:00:00"
      },
      {
        "start": "2020-04-24T13:00:00",
        "end": "2020-04-24T15:30:00"
      }
    ]

## Get list of all avaliable appointments by providing a objects (List<WorkingSchedule> and List<BookedAppointmets>) as  JSON strings

### Request

`GET /appointments/available`

    curl -X GET "https://localhost:44382/Appointments/available?workingSchedule=%5B%7B%22start%22%3A%222020-04-24%2008%3A00%3A00%22%2C%22end%22%3A%222020-04-24%2012%3A00%3A00%22%7D%2C%7B%22start%22%3A%222020-04-24%2013%3A00%3A00%22%2C%22end%22%3A%222020-04-24%2016%3A00%3A00%22%7D%5D&bookedAppointments=%5B%7B%22start%22%3A%222020-04-24%2009%3A00%3A00%22%2C%22end%22%3A%222020-04-24%2010%3A00%3A00%22%7D%2C%7B%22start%22%3A%222020-04-24%2015%3A30%3A00%22%2C%22end%22%3A%222020-04-24%2016%3A00%3A00%22%7D%5D" -H "accept: */*"

### Response

    content-length: 181 
    content-type: application/json; charset=utf-8 
    date: Sat, 02 May 2020 11:36:42 GMT 
    server: Microsoft-IIS/10.0 
    status: 200 
    x-powered-by: ASP.NET 

    [
      {
        "start": "2020-04-24T08:00:00",
        "end": "2020-04-24T09:00:00"
      },
      {
        "start": "2020-04-24T10:00:00",
        "end": "2020-04-24T12:00:00"
      },
      {
        "start": "2020-04-24T13:00:00",
        "end": "2020-04-24T15:30:00"
      }
    ]

## Get list of all avaliable for employee (real word scenario)

### Request

`GET /appointments/available/{employeeId}`

    curl -X GET "https://localhost:44382/Appointments/available/a98121e8-2492-43b2-9bce-289163324c0f" -H "accept: */*"

### Response

    content-length: 181 
    content-type: application/json; charset=utf-8 
    date: Sat, 02 May 2020 11:39:40 GMT 
    server: Microsoft-IIS/10.0 
    status: 200 
    x-powered-by: ASP.NET

    [
      {
        "start": "2020-04-24T08:00:00",
        "end": "2020-04-24T09:00:00"
      },
      {
        "start": "2020-04-24T10:00:00",
        "end": "2020-04-24T12:00:00"
      },
      {
        "start": "2020-04-24T13:00:00",
        "end": "2020-04-24T15:30:00"
      }
    ]
